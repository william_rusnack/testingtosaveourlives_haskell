import Test.Hspec

import Lib

import Data.List
import Test.Hspec
import Test.QuickCheck

main :: IO ()
main = hspec $ do
  describe "fizzBuzz" $ do
    describe "specific cases" $ do
      it "multiples of 3 (Fizz)" $ do
        fizzBuzz 3 `shouldBe` "Fizz"
        fizzBuzz 6 `shouldBe` "Fizz"

      it "multiples of 5 (Buzz)" $ do
        fizzBuzz 5 `shouldBe` "Buzz"
        fizzBuzz 10 `shouldBe` "Buzz"

      it "multiples of 3 & 5" $ do
        fizzBuzz 15 `shouldBe` "FizzBuzz"
        fizzBuzz 30 `shouldBe` "FizzBuzz"
      
      it "non-multiples" $ do
        fizzBuzz 1 `shouldBe` "1"
        fizzBuzz 2 `shouldBe` "2"

    describe "property cases" $ do
      it "multiples of 3 (Fizz)" $ property $
        isPrefixOf "Fizz" .
        fizzBuzz .
        (* 3) .
        getNonNegative

      xit "multiples of 5 (Buzz)" $ property $
        isSuffixOf "Buzz" .
        fizzBuzz .
        (* 5) .
        getNonNegative
        
      xit "multiples of 3 & 5" $ property $
        (== "FizzBuzz") .
        fizzBuzz .
        (* 15) .
        getNonNegative

      it "non-multiples" $
        forAll ( suchThat (getNonNegative <$> arbitrary)
                          (\num -> num `mod` 3 /= 0 &&
                                        num `mod` 5 /= 0 ) ) $
        \num -> fizzBuzz num == show num

  describe "fizz" $ do
    it "specific cases" $ do
      fizz 3 `shouldBe` Just "Fizz"
      fizz 6 `shouldBe` Just "Fizz"
      fizz 4 `shouldBe` Nothing

    -- property tests
    xit "multiples of 3" False
    xit "non-multiples of 3" False

  describe "buzz" $ do
    it "specific cases" $ do
      buzz 5 `shouldBe` Just "Buzz"
      buzz 10 `shouldBe` Just "Buzz"
      buzz 9 `shouldBe` Nothing

    -- property tests
    xit "multiples of 5" False
    xit "non-multiples of 5" False

