module Main where

import Lib

import Control.Monad (forM_)

main :: IO ()
main = do
  forM_ [1..100] $
    putStrLn . fizzBuzz

