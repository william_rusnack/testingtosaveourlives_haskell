module Lib where

import Data.Maybe

fizzBuzz :: Int -> String
fizzBuzz num = show num `fromMaybe` (fizz num <> buzz num)

fizz :: Int -> Maybe String
fizz num | num `mod` 3 == 0 = Just "Fizz"
         | otherwise = Nothing

buzz :: Int -> Maybe String
buzz num | num `mod` 5 == 0 = Just "Buzz"
         | otherwise  = Nothing

